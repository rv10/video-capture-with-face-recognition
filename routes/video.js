var express = require('express');
var router = express.Router();
var data = require('../videos.json')

/* GET users listing. */
router.get('/', function(req, res, next) {
  let id = req.query.id;
  let video = data.find((ele) => ele.id==id)
  res.render('video', { video });
});

module.exports = router;
