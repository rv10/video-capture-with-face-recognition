(function () {
  function buildQuiz() {
    // we'll need a place to store the HTML output
    const output = [];

    // for each question...
    myQuestions.forEach((currentQuestion, questionNumber) => {
      // we'll want to store the list of answer choices
      const answers = [];

      // and for each available answer...
      // for (letter in currentQuestion.answers) {
      //   // ...add an HTML radio button
      //   answers.push(
      //     `<label>
      //       <input type="radio" name="question${questionNumber}" value="${letter}">
      //       ${letter} :
      //       ${currentQuestion.answers[letter]}
      //     </label>`
      //   );
      // }

      answers.push(
        `<label>
            <input type="radio" name="question${questionNumber}" value="option_1">
            a :
            ${currentQuestion.option_1}
          </label>`
      );

      answers.push(
        `<label>
            <input type="radio" name="question${questionNumber}" value="option_2">
            b :
            ${currentQuestion.option_2}
          </label>`
      );

      answers.push(
        `<label>
            <input type="radio" name="question${questionNumber}" value="option_3">
            c :
            ${currentQuestion.option_3}
          </label>`
      );

      answers.push(
        `<label>
            <input type="radio" name="question${questionNumber}" value="option_4">
            d :
            ${currentQuestion.option_4}
          </label>`
      );

      // add this question and its answers to the output
      output.push(
        `<div class="question"> ${currentQuestion.question} </div>
          <div class="answers"> ${answers.join("")} </div>`
      );
    });

    // finally combine our output list into one string of HTML and put it on the page
    quizContainer.innerHTML = output.join("");
  }

  function showResults() {

    var name = document.getElementById("nameinput")
    console.log(name.value)

    if (name.value) {
      // gather answer containers from our quiz
      const answerContainers = quizContainer.querySelectorAll(".answers");

      // keep track of user's answers
      let numCorrect = 0;

      // for each question...
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // find selected answer
        const answerContainer = answerContainers[questionNumber];
        const selector = `input[name=question${questionNumber}]:checked`;
        const userAnswer = (answerContainer.querySelector(selector) || {}).value;

        // if answer is correct
        if (userAnswer === currentQuestion.answer) {
          // add to the number of correct answers
          numCorrect++;

          // color the answers green
          answerContainers[questionNumber].style.color = "lightgreen";
        } else {
          // if answer is wrong or blank
          // color the answers red
          answerContainers[questionNumber].style.color = "red";
        }
      });

      // show number of correct answers out of total
      resultsContainer.innerHTML = `${numCorrect} out of ${myQuestions.length}`;

      console.log("************************************************************here");


      var result = {
        "totalQuestions":myQuestions.length,
        "rightAnswers":numCorrect,
        "users_name":name.value
      }

      const OtherParams = {
        headers: {
          "content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(result),
        method: "POST"
      };
      fetch('/questions/answer', OtherParams).then(res => { console.log(res) });
      console.log("here");

      // TODO: Write api to save to db
    } else {
      alert("please enter your name")
    }


  }

  const quizContainer = document.getElementById("quiz");
  const resultsContainer = document.getElementById("results");
  const submitButton = document.getElementById("submit");
  const myQuestions = JSON.parse(document.getElementById("quest").value);

  console.log(document.getElementById("quest").value)

  // display quiz right away
  buildQuiz();

  // on submit, show results
  submitButton.addEventListener("click", showResults);
})();