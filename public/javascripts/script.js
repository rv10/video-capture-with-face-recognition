const cam = document.getElementById('cam')
const video = document.getElementById('video')

let stopped = true;

var status = "INITIAL";

var endOfLOOp = []

var faceObj = {
  "start": 0,
  "end" :0
}

function selectvideo(id) {
  window.location.href = "/video?id=" + id;
}

function start() {
  status = "INITIAL";
  endOfLOOp = []
  faceObj.start = 0
  faceObj.end = 0
  stopped = false;
  startCam()
}

function pause() {
  cam.pause();
  video.pause();
}

function resume() {
  cam.play();
  video.play();
}
function stop() {
  stopped = true;
  cam.srcObject.getTracks().map((val) => {
    val.stop();
  });

  console.log(endOfLOOp)
  getResult()
  video.pause();
  video.currentTime = 0;

  document.body.querySelector("canvas").remove()
}

function startCam() {
  Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
    faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
    faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
    faceapi.nets.faceExpressionNet.loadFromUri('/models')
  ]).then(startVideo)
}

function startVideo() {
  navigator.getUserMedia(
    { video: true },
    stream => cam.srcObject = stream,
    err => console.error(err)
  )

  video.play()
}


function getResult()
{
//   request({
//     url: "/questions",
//     method: "POST",
//     json: true,   // <--Very important!!!
//     body: endOfLOOp
// }, function (error, response, body){
//     console.log(response);
// });

document.getElementById("myHiddenField").value = JSON.stringify(endOfLOOp);
document.getElementById("myForm").submit();

// const OtherParams = {
//   headers : {
//     "content-type":"application/json; charset=UTF-8"
//   },
//   body : JSON.stringify(endOfLOOp),
//   method : "POST"
// };
// fetch('/questions',OtherParams).then(res=>{console.log(res)});

}

cam.onplay = function() {
  const canvas = faceapi.createCanvasFromMedia(cam)
  document.body.append(canvas)
  const displaySize = { width: cam.width, height: cam.height }
  faceapi.matchDimensions(canvas, displaySize)
  let b = setInterval(async () => {
    const detections = await faceapi.detectAllFaces(cam, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
    faceapi.draw.drawDetections(canvas, resizedDetections)
    // faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
    // faceapi.draw.drawFaceExpressions(canvas, resizedDetections)

    if (stopped) {
      clearInterval(b);
    }

    if(detections.length != 0)
    {
      console.log("face detected")
      console.log(status)
      if(status=="NO_FACE")
      {
        if(faceObj.start!=0)
        {
          console.log("time added")
          faceObj.end = video.currentTime
          let copyOA = Object.assign({}, faceObj);
          endOfLOOp.push(copyOA)
        }
        status = "FACE";
      }
    }

    if(detections.length == 0){
      console.log("no face face detected")
      console.log(status)
     // console.log(video.currentTime)
      if(status == "FACE" || status == "INITIAL")
      {
        console.log(video.currentTime)
        faceObj.start = video.currentTime
        status = "NO_FACE"
      }
    }
  }, 100)
}